// data de prueba
let data = [
    {id: 1, valor: 10, desc: 'uno'},
    {id: 2, valor: 20, desc: 'dos'},
    {id: 3, valor: 30, desc: 'tres'},
    {id: 4, valor: 40, desc: 'cuatro'},
    {id: 5, valor: 50, desc: 'cinco'},
  ]

const saludando = (texto) => {
    return "Hola "+texto
}

function test(texto) {
    return "Hola "+texto
}

// raiz de la API
exports.raiz = function(req, res) {
    res.send("ok")
}

// Recibe parametro por GET retorna el registro donde si campo id conicide con el id a filtrar
exports.filtroRegistro = function(req, res) {
    const result = data.filter(row => row.id === parseInt(req.query.id))
    res.send(result)
}

// Recibe parametro por GET retorna el True si coincide con el campo id de algun registro del array
// en caso contrario retorna False
exports.getSome = function(req, res) {
    const result = data.some(row => row.id === parseInt(req.query.id))
    res.send({respuesta: result})
}

// recorre todo el array y multiplica 10 a cada campo valor
exports.Multiplica = function(req, res) {
    let result = data.map( function(row){
        return {id: row.id, valor: row.valor * 10, desc: row.desc}
    })

    res.send(result)
}

// retirna la fecha actual formateada
exports.Now = function(req, res) {
    let now = Date.now()
    now = new Date(now)
    res.send({ahora: now.toDateString()})
}

// Recibe parametros POST y agrega un registro al array de prueba
exports.addRegistro = function(req, res) {
    const arr = [...data, {id: req.body.id, valor: req.body.valor, desc: req.body.desc}]
    //data.push({id: req.body.id, valor: req.body.valor, desc: req.body.desc})
    res.send(arr)
}

// Promise simple, espera 5 segundos
exports.setPromesa = function(req, res) {
    let promesa = new Promise((resolve, reject) => {
        setTimeout(function(){
          resolve("Todo OK")
        }, 5000)
    })
      
    promesa.then((data) => {
        res.send(data)
    })
}

// Diferentes maneras de manejar una condicion en un promise
exports.setPromesaError = function(req, res) {
    const dato = 7
    let promesa = new Promise((resolve, reject) => {
        setTimeout(function(){
            if(req.query.dato === dato)
                resolve("Dato es igual")
            else
                reject("Dato es diferente")

            /*
            if(req.query.dato == dato)
                resolve("Dato es igual")
            else
                reject("Dato es diferente")
            */

            /*
            if(parseInt(req.query.dato) === dato)
                resolve("Dato es igual")
            else
                reject("Dato es diferente")
            */

            
            //req.query.dato == dato ? resolve("Dato es igual") : reject("Dato es diferente")
            
        }, 5000)
    })
      
    promesa.then((info) => res.send({info}))
    .catch((error) => {
        res.status(500).send({ error })
    })
}

// Recorre el array de prueba e imprime en consola key: valor
exports.recorrerArray = function(req, res) {
    data.map( row => {
        for (const [key, value] of Object.entries(row)) {
            console.log(`${key}: ${value}`);
        }
        console.log("--------------")
    })

    res.send(data)
}

// ejecuta funcion externa
exports.funcionExterna = function(req, res) {
    const data = {saludo: saludando(req.query.texto)}
    res.send(data)
}


// Copiar array  ----------------------------------
// const data2 = [...data]

// Quitar duplicados ----------------------------------
// const data2 = [...new Set(data)]

// Concatenar arrays ----------------------------------
// const data1 = [1,2,3]
// const data2 = [4,5,6]
// const arr = [...data1, ...data2]

// Copiar objecto ----------------------------------
// const objecto = { descripcion: 'Hola Mundo' }
// const copia = { ...objecto }

// Concatenar objetos ----------------------------------
// const objecto1 = { descripcion: 'Buenos dias' }
// const objecto2 = { descripcion: 'Buenas tardes' }
// const objecto3 = { descripcion: 'Buenas noches' }
// const objetofull = { ...objecto1, ...objecto2, ...objecto3 }

// Dividir string y cada caracter es un elemento de un array ----------------------------------
// const descripcion = 'ENTRENAMIENTO'
// const arr = [...descripcion]

// Quitar un elemento
// const data = [1,2,3,4,5,6]
// const elimnar = 5
// const filtrado = data.filter(item => item !== eliminar)