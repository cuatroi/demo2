var express = require('express'), router = express.Router()
var metodos_controller = require('./metodos')

router.get('/', metodos_controller.raiz)
router.get('/getregistro', metodos_controller.filtroRegistro)
router.post('/addregistro', metodos_controller.addRegistro)
router.get('/setpromesa', metodos_controller.setPromesa)
router.get('/setpromesaerror', metodos_controller.setPromesaError)
router.get('/getsome', metodos_controller.getSome)
router.get('/multiplica', metodos_controller.Multiplica)
router.get('/now', metodos_controller.Now)
router.get('/recorrer', metodos_controller.recorrerArray)
router.get('/saludando', metodos_controller.funcionExterna)

module.exports = router