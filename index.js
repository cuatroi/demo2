const express = require('express')
const app = express()
const port = 8000

app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.use(require('./controllers'))

app.listen(port, () => {
  console.log(`Ejecutandose en la URL http://localhost:${port}`)
})